import 'package:flutter/material.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/theme/theme_value.dart';
import 'package:incident_manager/theme/themes.dart';
import 'package:provider/provider.dart';

import 'theme_notifier.dart';

class SettingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SettingState();
}

class _SettingState extends State<SettingScreen> {
  int _selectedPosition = -1;
  var isDarkTheme;
  List themes = [Themes.LIGHT, Themes.DARK];
  late ThemeNotifier themeNotifier;

  @override
  Widget build(BuildContext context) {
    isDarkTheme = Theme.of(context).brightness == Brightness.dark;
    themeNotifier = Provider.of<ThemeNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView.builder(
          itemBuilder: (context, position) {
            return _createList(context, themes[position], position);
          },
          itemCount: themes.length,
        ),
      ),
    );
  }

  _createList(context, item, position) {
    SharedPref.readInt(Themes.POS).then((value) => setState(() {
          _selectedPosition = value!;
        }));
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () {
        _updateState(position);
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Radio(
            value: _selectedPosition,
            groupValue: position,
            activeColor: isDarkTheme ? darkPink : textBlack,
            onChanged: (_) {
              _updateState(position);
            },
          ),
          Text(item),
        ],
      ),
    );
  }

  _updateState(int position) {
    setState(() {
      _selectedPosition = position;
    });
    SharedPref.saveInt(Themes.POS, position);
    onThemeChanged(themes[position]);
  }

  void onThemeChanged(String value) {
    setState(() {
      print(value);
      if (value == Themes.DARK) {
        themeNotifier.setTheme(AppTheme().darkTheme);
      } else {
        themeNotifier.setTheme(AppTheme().lightTheme);
      }
      SharedPref.saveString(Themes.APP_THEME, value);
    });
  }
}
