import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/theme/theme_value.dart';

class AppTheme {
  get darkTheme => ThemeData(
        primarySwatch: donatRed,
        primaryColor: donatRed,
        brightness: Brightness.dark,
        backgroundColor: donatMittelgrau02,
        accentColor: donatHellgrau,
        accentIconTheme: IconThemeData(color: donatDunkelgrau02),
        dividerColor: donatDunkelgrau02,
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: textGrey),
          labelStyle: TextStyle(color: white),
        ),
      );

  get lightTheme => ThemeData(
        primarySwatch: donatRed,
        primaryColor: donatRed,
        brightness: Brightness.light,
        backgroundColor: donatHellgrau,
        accentColor: donatDunkelgrau02,
        accentIconTheme: IconThemeData(color: donatMittelgrau),
        dividerColor: donatMittelgrau,
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: TextStyle(color: textGrey),
          labelStyle: TextStyle(color: white),
        ),
      );
}
