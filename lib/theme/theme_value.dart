import 'package:flutter/material.dart';

const Color textBlack = Colors.black;
const Color textGrey = Colors.grey;
const Color white = Colors.white;
const Color lightGreyDarkMode = Colors.grey;
const Color darkPink = Colors.pink;
const Color grey2 = Colors.blueGrey;

class Themes {
  static String DARK = " dark";
  static String LIGHT = "light";
  static String APP_THEME = "app theme";
  static String POS = "position";
}
