import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';

class CGridTile extends StatelessWidget {
  late final String title;
  late final int pos;
  late bool isAdmin;

  CGridTile(this.title, this.pos);

  Future<bool> Admin() async {
    isAdmin = await SharedPref.readBool(isAdminS) ?? false;
    return isAdmin;
  }

  @override
  Widget build(BuildContext context) {
    Admin();
    bool isDarktheme = Theme.of(context).brightness == Brightness.dark;
    return RawMaterialButton(
      child: Text(
        title,
        style: new TextStyle(color: Colors.white, fontSize: 14),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(
              color: isDarktheme ? donatRed : donatDunkelgrau, width: 2)),
      fillColor: isDarktheme ? donatMittelgrau : donatHellgrau,
      onPressed: () => _openPage(context),
    );
  }

  _openPage(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                isAdmin ? pageListAdmin[pos] : pageListEmployee[pos]));
  }
}
