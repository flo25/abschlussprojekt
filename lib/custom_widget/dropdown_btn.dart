import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';

//todo auch in dartsupportlibrary einbauen
class DropDownBtn extends StatefulWidget {
  String? dropDownValue;
  final List<String>? dropDownOptions;

  DropDownBtn({this.dropDownOptions, this.dropDownValue});

  @override
  _DropDownBtnState createState() => _DropDownBtnState();
}

class _DropDownBtnState extends State<DropDownBtn> {
  @override
  Widget build(BuildContext context) {
    bool isDarktheme = Theme.of(context).brightness == Brightness.dark;
    return Container(
      child: Center(
        child: DropdownButton<String>(
          value: widget.dropDownValue,
          onChanged: (String? newValue) {
            setState(() {
              widget.dropDownValue = newValue;
            });
          },
          style: TextStyle(color: donatRed),
          selectedItemBuilder: (BuildContext context) {
            return widget.dropDownOptions!.map((String? value) {
              return Text(
                widget.dropDownValue!,
                style:
                    TextStyle(color: isDarktheme ? Colors.white : Colors.black),
              );
            }).toList();
          },
          items: widget.dropDownOptions!
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
