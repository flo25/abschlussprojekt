class DBStrings {
  static const String USER = "users";
  static const String UID = "uID";
  static const String U_ROLE = "role";
  static const String U_LOCATION = "location";
  static const String U_NAME = "name";
  static const String U_NOTIFICATION = "notification";
  static const String U_TOKEN = "token";
  static const String INCIDENT = "incident";
  static const String I_ID = "incidentId";
  static const String I_TITEL = "titel";
  static const String I_DESCRIPTION = "description";
  static const String I_LOCATION = "location";
  static const String I_STARTTIME = "startTime";
  static const String I_ENDTIME = "endTime";
  static const String I_DATE = "date";
  static const String I_SENDER = "sender";
  static const String I_ISWARNING = "isWarning";
  static const String I_ADMINID = "adminID";
  static const String I_SENDTO = "sendTo";
  static const String I_SENDSTATE = "sendState";

  //Filter
  static const String I_STARTDATE = "startDate";
  static const String I_ENDDATE = "endDate";
}
