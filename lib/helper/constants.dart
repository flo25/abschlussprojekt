import 'package:flutter/material.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/model/llist.dart';
import 'package:incident_manager/page/admin_incidentview.dart';
import 'package:incident_manager/page/analyse.dart';
import 'package:incident_manager/page/report_incident.dart';
import 'package:incident_manager/page/settings.dart';
import 'package:incident_manager/services/server_helper.dart';

//CONSTANT COLORS
const Color donatBlack = Color(0xff000000);
const Color donatHellgrau = Color(0xffbfbfbf);
const Color donatHellgrau02 = Color(0xff8d8d8e);
const Color donatMittelgrau = Color(0xff7f7f7f);
const Color donatMittelgrau02 = Color(0xff3c3c3b);
const Color donatDunkelgrau = Color(0xff595959);
const Color donatDunkelgrau02 = Color(0xff282828);
const MaterialColor donatRed = MaterialColor(
  _donatRedValue,
  <int, Color>{
    50: Color(0xffce233c),
    100: Color(0xffce233c),
    200: Color(0xffce233c),
    300: Color(0xffce233c),
    400: Color(0xffce233c),
    500: Color(_donatRedValue),
    600: Color(0xffce233c),
    700: Color(0xffce233c),
    800: Color(0xffce233c),
    900: Color(0xffce233c),
  },
);
const int _donatRedValue = 0xffce233c;

Image warning = Image.asset('assets/icons/warning_round.svg');
Image ok = Image.asset('assets/icons/ok.svg');

String getNotificationIcon(bool pBool) {
  return pBool ? 'assets/icons/warning_round.svg' : 'assets/icons/ok.svg';
}

const String uID = "uid";
const String token = "token";
const String adminID = "adminID";
const String isAdminS = "isAdmin";
const String extraLocation = "extraLocation";
const String extraIncident = "extraIncidents";

// int getUID() {
//   int id = SharedPref.readInt(uID) as int;
//   return id;
// }

//  getUID1() async {
//   int id = await SharedPref.readInt(uID) ;
//   return id;
// }

Future<bool> isAdmin() async {
  bool isAdmin = await SharedPref.readBool(isAdminS) ?? false;
  return isAdmin;
}

int getAdminID() {
  int id = SharedPref.readInt(adminID) as int;
  return id;
}

//LISTS
List<Widget> pageListAdmin = <Widget>[
  ReportIncident(),
  Settings(),
  Analyse(), //todo api anbinden
  StoerungsView()
];

List<Widget> pageListEmployee = <Widget>[
  ReportIncident(),
  Settings(),
];

List<String> menuListAdmin = [
  "menu_incident",
  "menu_settings",
  "menu_analyse",
  "current_incidents"
];
List<String> menuListEmp = ["menu_incident", "menu_settings"];

List<String> incidents = [
  "hint_incident",
  "network_error",
  "mail_error",
  "phone_error",
  "no_power"
];
// List<String> incidentsOnly = [
//   "network_error",
//   "mail_error",
//   "phone_error",
//   "no_power"
// ];
late LList? llist;

LList? loadLList() {
  llist = ServerHelper.getList();
  return llist;
}

updateLList() {
  llist = ServerHelper.getList();
}

List<String>? incidentsOnly = llist!.incidents;
List<String>? locationsOnly = llist!.locations;

const List<String> locationCity = ["location", "ingolstadt", "garching"];
const List<String> locations = [
  "location",
  "ingolstadt",
  "garching",
  "homeoffice"
];
// List<String> locationsOnly = ["ingolstadt", "garching", "homeoffice"];
const List<String> roleList = ["hint_role", "admin", "employee"];

bool isWarning = false;

//Enums

enum Role { ADMIN, EMPLOYEE }

extension RoleExt on Role {
  String get name {
    switch (this) {
      case Role.ADMIN:
        return 'Administrator';
      case Role.EMPLOYEE:
        return 'Mitarbeiter';
      default:
        return "";
    }
  }
}

extension RoleGet on String {
  Role getString2Role() {
    if (this == Role.ADMIN.name) {
      return Role.ADMIN;
    } else {
      return Role.EMPLOYEE;
    }
  }
}

enum Location { ALL, INGOLSTADT, GARCHING, NONE }

extension LocationExt on Location {
  String get name {
    switch (this) {
      case Location.ALL:
        return 'All';
      case Location.INGOLSTADT:
        return 'Ingolstadt';
      case Location.GARCHING:
        return 'Garching';
      default:
        return "";
    }
  }
}

extension LocationGet on String {
  Location getString2Location() {
    if (this == Location.ALL.name) {
      return Location.ALL;
    } else if (this == Location.INGOLSTADT.name) {
      return Location.INGOLSTADT;
    } else {
      return Location.GARCHING;
    }
  }
}

enum Period { DAY, WEEK, MONTH, YEAR }
enum SendState { ADMIN, WARNING, ALL_CLEAR }
