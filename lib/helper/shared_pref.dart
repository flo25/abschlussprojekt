import 'package:shared_preferences/shared_preferences.dart';

///Class to save and read values from the device local shared preferences
///important shared prefs don´t replace a database so use it wisely
class SharedPref {
  final String initialVisit = 'initialVisitList';
  late bool dailyFirstOpen;
  static bool isFirstStart = false;

  SharedPref();

  //read methods
  ///method to save integers
  ///[mKey] should be an unique String as key
  static Future<int?> readInt(mKey) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt(mKey)!;
  }

  ///method to save Doubles
  ///[mKey] should be an unique String as key
  static Future<double?> readDouble(mKey) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getDouble(mKey) ?? null;
  }

  ///method to save String
  ///[mKey] should be an unique String as key
  static Future<String?> readString(mKey) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(mKey);
  }

  ///method to save StringList
  ///[mKey] should be an unique String as key
  static Future<List<String>?> readStringList(mKey) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(mKey) ?? null;
  }

  ///method to save Booleans
  ///[mKey] should be an unique String as key
  static Future<bool?> readBool(mKey) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(mKey) ?? null;
  }

  //save methods
  ///method to read Strings
  ///[mKey] should be an unique String as key
  ///[mValue] is the value which should be stored
  static saveString(mKey, mValue) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(mKey, mValue);
  }

  ///method to read Integers
  ///[mKey] should be an unique String as key
  ///[mValue] is the value which should be stored
  static saveInt(mKey, mValue) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(mKey, mValue);
  }

  ///method to read Booleans
  ///[mKey] should be an unique String as key
  ///[mValue] is the value which should be stored
  static saveBool(mKey, mValue) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(mKey, mValue);
  }

  ///method to read StringLists
  ///[mKey] should be an unique String as key
  ///[mValue] is the value which should be stored
  static saveStringList(mKey, mValue) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(mKey, mValue);
  }

  ///method to read Doubles
  ///[mKey] should be an unique String as key
  ///[mValue] is the value which should be stored
  static saveDouble(mKey, mValue) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble(mKey, mValue);
  }

  ///Check if the user opens the app for the first time
  static Future<void> checkForFirstStart() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getKeys().isEmpty) {
      print("First App Start");
      isFirstStart = true;
    }
  }
}
