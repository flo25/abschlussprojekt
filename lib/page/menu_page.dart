import 'package:flutter/material.dart';
import 'package:incident_manager/custom_widget/custom_grid_tile.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/language/translations.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  bool isAdmin = false;

  Future<bool> Admin() async {
    isAdmin = await SharedPref.readBool(isAdminS) ?? false;
    return isAdmin;
  }

  @override
  void setState(fn) {
    super.setState(fn);
    Admin();
  }

  //todo user mitarbeiter dann menutiles anpassen von der höhe her. momentan zu groß
  @override
  Widget build(BuildContext context) {
    Admin();
    // (context as Element).reassemble();
    print(isAdmin = true);
    return Scaffold(
      appBar: AppBar(
        // title: Text('home_page'.translate(context)),
        title: Text("Home"),
      ),
      body: GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, position) {
          return new Card(
              child: new CGridTile(
                  isAdmin
                      ? menuListAdmin[position].translate(context)
                      : menuListEmp[position].translate(context),
                  position));
        },
        itemCount: isAdmin ? pageListAdmin.length : pageListEmployee.length,
      ),
    );
  }
}
