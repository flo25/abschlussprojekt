import 'package:flutter/material.dart';
import 'package:incident_manager/custom_widget/dropdown_btn.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/user.dart';
import 'package:incident_manager/services/push_notification_manager.dart';
import 'package:incident_manager/services/server_helper.dart';

import 'menu_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    List<String> roles = new List.empty(growable: true);
    roleList.forEach((element) {
      roles.add(element.translate(context));
    });
    DropDownBtn role = DropDownBtn(
      dropDownValue: roles[0],
      dropDownOptions: roles,
    );
    List<String> locs = new List.empty(growable: true);
    locationCity.forEach((element) {
      locs.add(element.translate(context));
    });
    DropDownBtn location = DropDownBtn(
      dropDownValue: locs[0],
      dropDownOptions: locs,
    );
    return Scaffold(
        appBar: AppBar(
          title: Text('app_name'.translate(context)),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                controller: _myController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(18.00, 13.0, 18.0, 13.0),
                  hintText: 'enter_new_item'.translate(context),
                ),
                // autofocus: true,
                // onEditingComplete: () => _addItem(context),
              ),
              SizedBox(
                height: 50,
              ),
              location,
              SizedBox(
                height: 50,
              ),
              role,
              SizedBox(
                height: 50,
              ),
              FlatButton(
                child: Text("save".translate(context)),
                onPressed: () => _onSave(
                    context, role.dropDownValue!, location.dropDownValue!),
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: donatRed, width: 1, style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(50)),
              )
            ],
          ),
        ));
  }

  _onSave(BuildContext context, String? role, String? location) {
    int id = _generateUID();
    ServerHelper.createUser(id, _myController.text, role, true, location,
        PushNotificationsManager.token);
    SharedPref.saveInt(uID, id);
    SharedPref.saveString(token, PushNotificationsManager.token);
    print(PushNotificationsManager.token);
    if (role == Role.ADMIN.name) {
      SharedPref.saveInt(adminID, id);
      SharedPref.saveBool(isAdminS, true);
      print("ist Admin"); //fix is admin noch nicht richtig abgebspeicher
    }
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MenuPage()));
  }

  int _generateUID() {
    List<User>? users = ServerHelper.getUsers(null, null, null, null);
    return users == null ? 1 : users.length + 1;
  }
}
