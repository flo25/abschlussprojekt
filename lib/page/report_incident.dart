import 'package:flutter/material.dart';
import 'package:incident_manager/custom_widget/dropdown_btn.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/incident.dart';
import 'package:incident_manager/model/user.dart';
import 'package:incident_manager/services/server_helper.dart';

class ReportIncident extends StatefulWidget {
  @override
  _ReportIncidentState createState() => _ReportIncidentState();
}

class _ReportIncidentState extends State<ReportIncident> {
  final _controller = TextEditingController();
  static const int MAX_DESC_LENGTH = 300;
  static const double DIVIDER_HEIGHT = 10;
  late int id;

  @override
  Widget build(BuildContext context) {
    getUID();
    List<String> incidentList = List.empty(growable: true);
    incidents.forEach((element) {
      incidentList.add(element.translate(context));
    });
    List<String> locationList = List.empty(growable: true);
    locations.forEach((element) {
      locationList.add(element.translate(context));
    });
    DropDownBtn incident = DropDownBtn(
      dropDownValue: "hint_incident".translate(context),
      dropDownOptions: incidentList,
    );
    DropDownBtn location = DropDownBtn(
      dropDownValue: "location".translate(context),
      dropDownOptions: locationList,
    );
    return Scaffold(
        appBar: AppBar(
          title: Text('menu_incident'.translate(context)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              incident,
              SizedBox(
                height: DIVIDER_HEIGHT,
              ),
              TextField(
                controller: _controller,
                keyboardType: TextInputType.multiline,
                maxLines: 5,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'description'.translate(context),
                ),
                maxLength: MAX_DESC_LENGTH,
                autofocus: false,
              ),
              SizedBox(
                height: DIVIDER_HEIGHT,
              ),
              location,
              SizedBox(
                height: DIVIDER_HEIGHT,
              ),
              FlatButton(
                child: Text("report".translate(context),
                    style: TextStyle(color: Colors.white)),
                onPressed: () => _onReport(
                    context, incident.dropDownValue!, location.dropDownValue!),
                color: donatRed,
              )
            ],
          ),
        ));
  }

  Future<int> getUID() async {
    id = await SharedPref.readInt(uID) ?? 1;
    return id;
  }

  _onReport(BuildContext context, String incident, String location) {
    User? currentUser = ServerHelper.getUser(id);
    currentUser ?? ServerHelper.getUser(id);
    DateTime now = DateTime.now();

    ServerHelper.createIncident(
        _generateIncidentID(),
        incident,
        _controller.text,
        location,
        now,
        now,
        now,
        currentUser!.name!,
        false,
        -1,
        -1,
        SendState.ADMIN.index);
  }

  int _generateIncidentID() {
    List<Incident>? incidents = ServerHelper.getIncidents(
        null, null, null, null, null, null, null, null, null, null, -1);
    return incidents == null ? 1 : incidents.length + 1;
  }
}
