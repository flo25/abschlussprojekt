// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:incident_manager/custom_widget/multi_select_dialog.dart';
// import 'package:incident_manager/helper/constants.dart';
// import 'package:incident_manager/language/translations.dart';
// import 'package:intl/intl.dart';
//
// class Filter extends StatefulWidget{
//   // List<String> selectedIncidents = List();
//   // List<String> selectedLocations = List();
//   // Period period;
//   // String fromDate;
//   // String toDate;
//
//   @override
// _FilterState createState() => _FilterState();
// }
//
// class _FilterState extends State<Filter>{
//   final textStyleDate = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
//   static const double SPACING_HEIGHT = 5;
//   static const double SPACING_WIDTH = 20;
//
//   List<String> _selectedIncidents = List();
//   List<int> _sortLocation = List();
//   List<String> _selectedLocations = List();
//   List<int> _sortIncident = List();
//   DateTime selectedDate = DateTime.now();
//   int _radioValue = 0;
//   Period period;
//   String fromDate;
//   String toDate;
//
//   @override
//   Widget build(BuildContext context) {
//     // widget.toDate = toDate;
//     // widget.fromDate = fromDate;
//     // widget.period = period;
//     // widget.selectedLocations = _selectedLocations;
//     // widget.selectedIncidents = _selectedIncidents;
//     return  Scaffold(
//       appBar: AppBar(
//         title: Text("filter".translate(context)),
//       ),
//       body: Container(
//         child: Column(
//           children: [
//             Center(
//               child: Row(
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   GestureDetector(
//                     child: Text(fromDate ?? "fromDate".translate(context),
//                         style: textStyleDate),
//                     onTap: () => _openDatePicker(context, 0),
//                   ),
//                   Text("    -    ", style: textStyleDate),
//                   GestureDetector(
//                     child: Text(toDate ?? "toDate".translate(context),
//                         style: textStyleDate),
//                     onTap: () => _openDatePicker(context, 1),
//                   ),
//                 ],
//               ),
//             ),
//             SizedBox(
//               height: SPACING_HEIGHT,
//             ),
//             FlatButton(
//               child: Text("hint_incident".translate(context)),
//               onPressed: () => _showMultiSelect(context, incidentsOnly, true),
//             ),
//             SizedBox(
//               height: SPACING_HEIGHT,
//             ),
//             FlatButton(
//               child: Text("location".translate(context)),
//               onPressed: () =>
//                   _showMultiSelect(context, locationsOnly, false),
//             ),
//             SizedBox(
//               height: SPACING_HEIGHT,
//             ),
//             Column(
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Row(children: [
//                     new Radio(
//                       value: 0,
//                       groupValue: _radioValue,
//                       onChanged: (value) =>
//                           _handleRadioValueChange(value, context),
//                     ),
//                     new Text('day'.translate(context)),
//                     new Radio(
//                       value: 1,
//                       groupValue: _radioValue,
//                       onChanged: (value) =>
//                           _handleRadioValueChange(value, context),
//                     ),
//                     new Text('week'.translate(context)),
//                   ]),
//                   Row(children: [
//                     new Radio(
//                       value: 2,
//                       groupValue: _radioValue,
//                       onChanged: (value) =>
//                           _handleRadioValueChange(value, context),
//                     ),
//                     new Text('month'.translate(context)),
//                     new Radio(
//                       value: 3,
//                       groupValue: _radioValue,
//                       onChanged: (value) =>
//                           _handleRadioValueChange(value, context),
//                     ),
//                     new Text('year'.translate(context)),
//                   ]),
//                 ]),
//             SizedBox(
//               height: SPACING_HEIGHT,
//             ),
//             Row(
//               children: [
//                 Spacer(),
//                 FlatButton(
//                   onPressed: () => _popDialog(context),
//                   child: Text("cancel".translate(context)),
//                 ),
//                 SizedBox(
//                   width: SPACING_WIDTH,
//                 ),
//                 FlatButton(
//                   onPressed: () => _closeDialog(context),
//                   child: Text("ok".translate(context)),
//                 )
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   void _handleRadioValueChange(int value, BuildContext context) {
//     setState(() {
//       _radioValue = value;
//
//       switch (_radioValue) {
//         case 0:
//           period = Period.DAY;
//           break;
//         case 1:
//           period = Period.WEEK;
//           break;
//         case 2:
//           period = Period.MONTH;
//           break;
//         case 3:
//           period = Period.YEAR;
//           break;
//       }
//       //refreshes the state
//       (context as Element).reassemble();
//     });
//   }
//
//   ///Schließt den Dialog und wendet die Filter an
//   _closeDialog(BuildContext context) {
//     Navigator.pop(context);
//     //todo diagramm mit den gefilterten angaben anzeigen
//     // setState(() {
//     //   widget.toDate = toDate;
//     //   widget.fromDate = fromDate;
//     //   widget.period = period;
//     //   widget._selectedLocations = _selectedLocations;
//     //   widget._selectedIncidents = _selectedIncidents;
//     // });
//   }
//
//   _popDialog(BuildContext context) {
//     Navigator.pop(context);
//   }
//
//
//   ///Datepickerdialog zur auswahl des zu filternden Zeitraumes
//   Future<void> _openDatePicker(BuildContext context, int i) async {
//     DateFormat formatter = DateFormat('dd.MM.yyyy');
//     final DateTime picked = await showDatePicker(
//         context: context,
//         initialDate: selectedDate,
//         firstDate: DateTime(2015, 8),
//         lastDate: DateTime(2101));
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         switch (i) {
//           case 0:
//             fromDate = formatter.format(picked);
//             break;
//           case 1:
//             toDate = formatter.format(picked);
//             break;
//         }
//         (context as Element).reassemble();
//       });
//   }
//
//   ///Zeigt den Multiselectdialog an um mehrere Störungen für die Filterung
//   ///auswählen zu können.
//   _showMultiSelect(
//       BuildContext context, List<String> pList, bool isIncident) async {
//     if (isIncident) {
//       List<MultiSelectDialogItem<int>> items = List();
//       int i = 0;
//       pList.forEach((element) {
//         items.add(MultiSelectDialogItem(i++, element.translate(context)));
//       });
//
//       _sortIncident = await showDialog<List<int>>(
//         context: context,
//         builder: (BuildContext context) {
//           return MultiSelectDialog(
//             items: items,
//             initialSelectedValues: _sortIncident,
//           );
//         },
//       );
//
//       _sortIncident.forEach((element) {
//         _selectedIncidents.add(incidents[element]);
//       });
//     } else {
//       List<MultiSelectDialogItem<int>> items = List();
//       int i = 0;
//       pList.forEach((element) {
//         items.add(MultiSelectDialogItem(i++, element.translate(context)));
//       });
//
//       _sortLocation = await showDialog<List<int>>(
//         context: context,
//         builder: (BuildContext context) {
//           return MultiSelectDialog(
//             items: items,
//             initialSelectedValues: _sortLocation,
//           );
//         },
//       );
//
//       _sortLocation.forEach((element) {
//         _selectedLocations.add(incidents[element]);
//       });
//     }
//   }
// }
