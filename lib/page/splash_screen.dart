import 'dart:async';

import 'package:flutter/material.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/page/login_page.dart';
import 'package:incident_manager/page/menu_page.dart';

///class shown on every startup from app
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final int duration = 1;
  bool firstStart = false;

  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: duration),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => _openPageFromState())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // Image.asset("assets/icon/icon.jpeg"),
              SizedBox(
                height: 30,
              ),
              // Image.asset("assets/icon/logo_writing.jpeg"),
            ],
          ),
        ),
      ),
    );
  }

  _openPageFromState() {
    print(SharedPref.isFirstStart.toString() + " ISFIRSTSTART VALUE");
    if (SharedPref.isFirstStart) {
      return LoginPage();
    } else {
      return MenuPage();
    }
  }

}
