import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/incident.dart';
import 'package:incident_manager/model/user.dart';
import 'package:incident_manager/services/server_helper.dart';

class StoerungsView extends StatefulWidget {
  @override
  _StoerungsViewState createState() => _StoerungsViewState();
}

class _StoerungsViewState extends State<StoerungsView> {
  static const double SPACING_HEIGHT = 25;
  static const double SPACING_WIDTH = 30;

  late Incident _incident;
  int _radioValue = 0;
  Location _sendTo = Location.NONE;
  int? id;
  int? uid;

  // int getAdminID() {
  //   id = SharedPref.readInt(adminID) as int;
  //   return id;
  // }

  Future<int?> getUID() async {
    uid = await SharedPref.readInt(uID) ?? null;
    return uid == null ? 1 : uid;
  }

  @override
  void setState(fn) {
    super.setState(fn);
    // getAdminID();
    // (context as Element).reassemble();
  }

  @override
  Widget build(BuildContext context) {
    List<Incident>? incidents = getTodaysIncidents(context);
    getUID();
    User? user;
    return Scaffold(
        appBar: AppBar(
          title: Text('current_incidents'.translate(context)),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              // height: MediaQuery.of(context).size.height,
              child: ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: incidents == null ? 1 : incidents.length,
                itemBuilder: (BuildContext context, int index) {
                  if (incidents != null)
                    user = ServerHelper.getUser(incidents[index].adminId!);
                  return GestureDetector(
                    child: Container(
                      height: 50,
                      child: Text(incidents != null
                          ? '${incidents[index].titel} seit ${incidents[index].startTime} \nBearbeiter: ${user == null ? 'noch kein Bearbeiter' : user!.name}'
                          : "Heute noch keine Störung"),
                    ),
                    onTap: () => incidents == null
                        ? _nothing(context)
                        : _showStoerungDetails(context, incidents[index]),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              ),
            ),
          ],
        ));
  }

  List<Incident>? getTodaysIncidents(BuildContext context) {
    DateTime date = DateTime.now();
    return ServerHelper.getIncidents(
        null, null, null, null, null, date, null, null, null, null, -1);
  }

  _showStoerungDetails(BuildContext context, Incident incident) {
    _incident = incident;
    Widget detailScreen = StatefulBuilder(builder: (context, setState) {
      return Scaffold(
          appBar: AppBar(
            title: Text("detail".translate(context)),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(incident.titel!),
                SizedBox(
                  height: 10,
                ),
                Text(incident.description!),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(incident.startTime),
                    Text("    -    "),
                    Text((incident.endTime == incident.startTime
                        ? ""
                        : incident.endTime)),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text("${"sender".translate(context)} : ${incident.sender}"),
                SizedBox(
                  height: 10,
                ),
                Text("${"location".translate(context)} : ${incident.location}"),
                SizedBox(
                  height: SPACING_HEIGHT,
                ),
                Text("send_to".translate(context)),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Column(children: [
                    new Radio(
                      value: 0,
                      groupValue: _radioValue,
                      onChanged: (int? value) =>
                          _handleRadioValueChange(value, context),
                    ),
                    new Text('all'.translate(context)),
                    new Radio(
                      value: 1,
                      groupValue: _radioValue,
                      onChanged: (int? value) =>
                          _handleRadioValueChange(value, context),
                    ),
                    new Text('ingolstadt'.translate(context)),
                    new Radio(
                      value: 2,
                      groupValue: _radioValue,
                      onChanged: (int? value) =>
                          _handleRadioValueChange(value, context),
                    ),
                    new Text('garching'.translate(context)),
                  ]),
                ),
                SizedBox(
                  height: SPACING_HEIGHT,
                ),
                Row(
                  children: [
                    Spacer(),
                    FlatButton(
                      onPressed: () => _popDialog(context),
                      child: Text("cancel".translate(context)),
                    ),
                    SizedBox(
                      width: SPACING_WIDTH,
                    ),
                    FlatButton(
                      onPressed: () => _sendNotification(
                          context, incident.warning!, incident.incidentID!),
                      child: Text(!incident.warning!
                          ? "send_warning".translate(context)
                          : "send_nowarning".translate(context)),
                    ),
                    Spacer(),
                  ],
                )
              ],
            ),
          ));
    });

    showGeneralDialog(
        context: context,
        barrierDismissible: false,
        pageBuilder: (_, __, ___) {
          return SizedBox.expand(
            child: detailScreen,
          );
        });
  }

  void _handleRadioValueChange(int? value, BuildContext context) {
    setState(() {
      _radioValue = value!;
      switch (_radioValue) {
        case 0:
          _sendTo = Location.ALL;
          break;
        case 1:
          _sendTo = Location.INGOLSTADT;
          break;
        case 2:
          _sendTo = Location.GARCHING;
          break;
      }
      (context as Element).reassemble();
    });
  }

  _sendNotification(BuildContext context, bool pIsWarning, int iId) {
    isWarning = !pIsWarning;
    ServerHelper.updateIncident(
        iId,
        null,
        null,
        null,
        null,
        isWarning ? null : DateTime.now(),
        null,
        null,
        isWarning,
        uid,
        this._sendTo.index,
        isWarning ? SendState.WARNING.index : SendState.ALL_CLEAR.index);
    Navigator.pop(context);
  }

  _popDialog(BuildContext context) {
    Navigator.pop(context);
  }

  _nothing(BuildContext context) {}
}
