import 'package:flutter/material.dart';
import 'package:incident_manager/charts/groupd_bar_chart.dart';
import 'package:incident_manager/charts/pie_chart.dart';
import 'package:incident_manager/custom_widget/multi_select_dialog.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/incident.dart';
import 'package:incident_manager/model/user.dart';
import 'package:incident_manager/services/server_helper.dart';
import 'package:intl/intl.dart';

class Analyse extends StatefulWidget {
  @override
  _AnalyseState createState() => _AnalyseState();
}

class _AnalyseState extends State<Analyse> {
  static const double SPACING_HEIGHT = 5;
  static const double SPACING_WIDTH = 20;
  final textStyleDate = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  late List<int> _sortIncident = List.empty(growable: true);
  late List<String> _selectedIncidents = List.empty(growable: true);
  late List<int> _sortLocation = List.empty(growable: true);
  late List<String> _selectedLocations = List.empty(growable: true);
  DateTime selectedDate = DateTime.now();
  bool _isGraphics = true;
  int _radioValue = 0;
  Period? period;
  String? fromDate;
  String? toDate;
  DateTime? _fromDate;
  DateTime? _toDate;
  String? locationSelected;
  String? incidentsSelected;
  bool? isFilterClosed = false;
  BuildContext? _context;

  @override
  Widget build(BuildContext context) {
    loadLList();
    _context = context;
    return Scaffold(
      appBar: AppBar(
        title: Text('menu_analyse'.translate(context)),
      ),
      body: Column(
        children: [
          Row(children: [
            Switch(
              value: _isGraphics,
              onChanged: (value) {
                setState(() {
                  _isGraphics = value;
                  (context as Element).reassemble();
                });
              },
              activeTrackColor: donatDunkelgrau,
              activeColor: donatRed,
            ),
            Text(_isGraphics
                ? "graphics".translate(context)
                : "table".translate(context)),
            Spacer(),
            TextButton(
              child: Text("filter".translate(context)),
              onPressed: () => {
                _showFitlerDialog(context),
                (context as Element).reassemble()
              },
            ),
            Spacer(),
          ]),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.8,
            child: _showGraphicOrTable(context),
          )
        ],
      ),
    );
  }

//todo period anzeige einbauen
  Widget _showGraphicOrTable(BuildContext context) {
    if (_isFilterEmpty()) {
      List<Incident>? inci = ServerHelper.getIncidents(
          null, null, null, null, null, null, null, null, null, null, null);
      inci = ServerHelper.getIncidents(
          null, null, null, null, null, null, null, null, null, null, null);
      print("Filter Empty");
      return _getPieChart(context, inci);
    } else {
      List<Incident>? incidents = List.empty(growable: true);
      if (isFilterClosed!) incidents = _getFilterResult();
      if (incidents == null || incidents.isEmpty) {
        return Text("no_filter_result".translate(context));
      } else {
        if (_isGraphics) {
          return StackedBarChart(
            StackedBarChart.createBarChart(context, incidents),
            animate: false,
          ); //get a bar chart wit x location and y percentage incidents
        } else {
          return _getListView(context, incidents);
        }
      }
    }
  }

  _popDialog(BuildContext context) {
    Navigator.pop(context);
  }

  _popDialogFilterClose(BuildContext context) {
    setState(() {
      isFilterClosed = true;
      (_context as Element).reassemble();
      (context as Element).reassemble();
    });
    Navigator.pop(context);
    _showGraphicOrTable(context);
  }

  //Filter methods

  _showFitlerDialog(BuildContext context) {
    setState(() {
      isFilterClosed = false;
    });
    bool update = false;
    Widget detailScreen = StatefulBuilder(builder: (context, setState) {
      return Scaffold(
        appBar: AppBar(
          title: Text("filter".translate(context)),
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      child: Text(fromDate ?? "fromDate".translate(context),
                          style: textStyleDate),
                      onTap: () => _openDatePicker(context, 0),
                    ),
                    Text("    -    ", style: textStyleDate),
                    GestureDetector(
                      child: Text(toDate ?? "toDate".translate(context),
                          style: textStyleDate),
                      onTap: () => _openDatePicker(context, 1),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: SPACING_HEIGHT,
              ),
              TextButton(
                child: Text((incidentsSelected == null
                    ? "hint_incident".translate(context)
                    : incidentsSelected)!),
                onPressed: () =>
                    _showMultiSelect(context, incidentsOnly!, true),
              ),
              SizedBox(
                height: SPACING_HEIGHT,
              ),
              TextButton(
                child: Text((locationSelected == null
                    ? "location".translate(context)
                    : locationSelected)!),
                onPressed: () =>
                    _showMultiSelect(context, locationsOnly!, false),
              ),
              SizedBox(
                height: SPACING_HEIGHT,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Spacer(),
                  TextButton(
                    onPressed: () => _popDialog(context),
                    child: Text("cancel".translate(context)),
                  ),
                  SizedBox(
                    width: SPACING_WIDTH,
                  ),
                  TextButton(
                    onPressed: () =>
                        {_popDialogFilterClose(context), update = true},
                    child: Text("ok".translate(context)),
                  ),
                  Spacer(),
                ],
              )
            ],
          ),
        ),
      );
    });

    showGeneralDialog(
        context: context,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (_, __, ___) {
          return SizedBox.expand(
            child: detailScreen,
          );
        });
    setState(() {
      update ? (context as Element).reassemble() : null;
    });
  }

  ///Datepickerdialog zur auswahl des zu filternden Zeitraumes
  Future<void> _openDatePicker(BuildContext context, int i) async {
    DateFormat formatter = DateFormat('dd.MM.yyyy');
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        switch (i) {
          case 0:
            _fromDate = picked;
            fromDate = formatter.format(picked);
            break;
          case 1:
            _toDate = picked;
            toDate = formatter.format(picked);
            break;
        }
        (context as Element).reassemble();
      });
  }

  ///Zeigt den Multiselectdialog an um mehrere Störungen für die Filterung
  ///auswählen zu können.
  _showMultiSelect(
      BuildContext context, List<String> pList, bool isIncident) async {
    StringBuffer sb = StringBuffer();
    List<MultiSelectDialogItem<int>> items = List.empty(growable: true);
    int i = 0;
    pList.forEach((element) {
      items.add(MultiSelectDialogItem(i++, element));
    });
    if (isIncident) {
      _sortIncident = (await showDialog<List<int>>(
        context: context,
        builder: (BuildContext context) {
          return MultiSelectDialog(
            items: items,
            initialSelectedValues: _sortIncident,
            dialogTitel: "select_incident".translate(context),
          );
        },
      ))!;
      _selectedIncidents.clear();
      sb.clear();
      _sortIncident.forEach((element) {
        _selectedIncidents.add(pList[element]);
      });
      _selectedIncidents.forEach((element) {
        sb.write(element);
        if (_selectedIncidents.length > 1) sb.write(",");
      });
      setState(() {
        incidentsSelected = "";
        if (_selectedIncidents.length > 1) {
          incidentsSelected = sb.toString().substring(0, sb.length - 1);
        } else {
          incidentsSelected = sb.toString();
        }
        (context as Element).reassemble();
      });
    } else {
      _sortLocation = (await showDialog<List<int>>(
        context: context,
        builder: (BuildContext context) {
          return MultiSelectDialog(
            items: items,
            initialSelectedValues: _sortLocation,
            dialogTitel: "select_locations".translate(context),
          );
        },
      ))!;

      _selectedLocations.clear();
      sb.clear();
      _sortLocation.forEach((element) {
        _selectedLocations.add(pList[element]);
      });
      _selectedLocations.forEach((element) {
        sb.write(element);
        if (_selectedLocations.length > 1) sb.write(",");
      });
      setState(() {
        locationSelected = "";
        if (_selectedLocations.length > 1) {
          locationSelected = sb.toString().substring(0, sb.length - 1);
        } else {
          locationSelected = sb.toString();
        }
        (context as Element).reassemble();
      });
    }
  }

  List<Incident>? _getFilterResult() {
    List<Incident>? incidents = List.empty(growable: true);
    if (_selectedLocations.isEmpty && _selectedIncidents.isEmpty) {
      //date filterung
      print("date filterung");
      return ServerHelper.getIncidents(null, null, null, null, null, null,
          _fromDate, _toDate, null, null, null);
    } else {
      if (_selectedIncidents.isNotEmpty && _selectedLocations.isEmpty) {
        // titel(mit datum)
        ServerHelper.getIncidents(null, null, null, null, null, null, _fromDate,
                _toDate, null, null, null)!
            .forEach((element) {
          if (_selectedIncidents.contains(element.titel))
            incidents.add(element);
        });
      } else if (_selectedIncidents.isEmpty && _selectedLocations.isNotEmpty) {
        //location(mit datum)
        ServerHelper.getIncidents(null, null, null, null, null, null, _fromDate,
                _toDate, null, null, null)!
            .forEach((element) {
          if (_selectedLocations.contains(element.location))
            incidents.add(element);
        });
      } else {
        //titel und location(mit datum)
        // List<Incident> inci = List();
        ServerHelper.getIncidents(null, null, null, null, null, null, _fromDate,
                _toDate, null, null, null)!
            .forEach((element) {
          if (_selectedLocations.contains(element.location) &&
              _selectedIncidents.contains(element.titel))
            incidents.add(element);
        });
        // if (inci != null) incidents.addAll(inci);
      }
    }
    return incidents;
  }

  bool _isFilterEmpty() {
    return toDate == null &&
        fromDate == null &&
        _sortIncident.isEmpty &&
        _sortLocation.isEmpty;
  }

  //Zeigt ein PieChart mit allen Vorfällen und deren Prozentigen anteilen
  Widget _getPieChart(BuildContext context, List<Incident>? incidents) {
    if (incidents == null) (_context as Element).reassemble();
    return PieChartLabel(
      PieChartLabel.createData(incidents!, context),
      animate: false,
    );
  }

  Widget _getListView(BuildContext context, List<Incident> incidents) {
    (_context as Element).reassemble();
    (context as Element).reassemble();
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: incidents.length,
      itemBuilder: (BuildContext context, int index) {
        User user = ServerHelper.getUser(incidents[index].adminId!)!;
        print(
            "Get List View: ${incidents[index].titel}  incidentID: ${incidents[index].incidentID}");
        return GestureDetector(
          child: Container(
            height: 50,
            child: Text(
                '${incidents[index].titel} - ${incidents[index].date}  \nAdministrator: ${user == null ? 'noch kein Bearbeiter' : user.name}'),
          ),
          onTap: () => _showStoerungDetails(context, incidents[index]),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

  _showStoerungDetails(BuildContext context, Incident incident) {
    Duration duration = ServerHelper.timeFormatter
        .parse(incident.endTime)
        .difference(ServerHelper.timeFormatter.parse(incident.startTime));
    Widget detailScreen = StatefulBuilder(builder: (context, setState) {
      return Scaffold(
          appBar: AppBar(
            title: Text("detail".translate(context)),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(incident.titel!),
                SizedBox(
                  height: 10,
                ),
                Text(incident.description!),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(incident.startTime),
                    Text("    -    "),
                    Text(incident.endTime),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                    "${"duration".translate(context)} : ${duration.inMinutes / 60} Std."),
                SizedBox(
                  height: 10,
                ),
                Text("${"sender".translate(context)} : ${incident.sender}"),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: SPACING_HEIGHT,
                ),
                Text("${"location".translate(context)}: ${incident.location}"),
                SizedBox(
                  height: SPACING_HEIGHT,
                ),
                Align(
                  alignment: AlignmentDirectional.bottomEnd,
                  child: FlatButton(
                    onPressed: () => _popDialog(context),
                    child: Text("ok".translate(context)),
                  ),
                )
              ],
            ),
          ));
    });

    showGeneralDialog(
        context: context,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (_, __, ___) {
          return SizedBox.expand(
            child: detailScreen,
          );
        });
  }
}
