import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/shared_pref.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/user.dart';
import 'package:incident_manager/services/server_helper.dart';
import 'package:incident_manager/theme/setting_screen.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool notification = true;
  bool isAdmin = false;
  bool theme = false; //später durch datenbankabfrage aktuallisieren
  List<String> addedLocations = new List.empty(growable: true);
  List<String> addedIncidents = new List.empty(growable: true);
  final _myController = TextEditingController();
  int? id;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _myController.dispose();
    super.dispose();
  }

  //fix wird nicht gleich beim erstenmal aufgerufen
  Future<int?> getUID() async {
    id = await SharedPref.readInt(uID) ?? 1;
    return id == null ? 1 : id;
  }

  //todo neue Vorfälle und Standorte durch Admin Hinzufügen können
  Future<bool> Admin() async {
    isAdmin = await SharedPref.readBool(isAdminS) ?? false;
    return isAdmin;
  }

  @override
  void setState(fn) {
    super.setState(fn);
    getUID();
    Admin();
    print(id);
    (context as Element).reassemble();
  }

  @override
  Widget build(BuildContext context) {
    getUID();
    Admin();
    print(id);
    User? currentUser = ServerHelper.getUser(id!);
    notification = currentUser!.notification!;
    Widget admin;
    if (isAdmin) {
      admin = _getAdder(context);
    } else {
      admin = SizedBox();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('menu_settings'.translate(context)),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Switch(
                value: notification,
                onChanged: (value) {
                  setState(() {
                    notification = value;
                    ServerHelper.updateUser(
                        getUID() as int, null, null, null, notification, null);
                  });
                },
                activeTrackColor: donatDunkelgrau,
                activeColor: donatRed,
                inactiveTrackColor: donatDunkelgrau,
                inactiveThumbColor: donatMittelgrau02,
              ),
              Text("notification".translate(context))
            ],
          ),
          Row(
            children: [
              GestureDetector(
                child: Text("theme".translate(context)),
                onTap: () => _onThemeDialog(context),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              admin,
            ],
          ),
        ],
      ),
    );
  }

  _onThemeDialog(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SettingScreen()));
  }

  Widget _getAdder(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Text("add_location".translate(context)),
          onTap: () => _onThemeDialog(context),
        ),
        SizedBox(
          height: 10,
        ),
        GestureDetector(
          child: Text("add_incident".translate(context)),
          onTap: () => _onThemeDialog(context),
        )
      ],
    );
  }

  _openAddDialog(BuildContext context, bool isIncident) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(isIncident
                ? "add_incident".translate(context)
                : 'add_location'.translate(context)),
            actions: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: TextField(
                  controller: _myController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.fromLTRB(18.00, 13.0, 18.0, 13.0),
                    hintText: 'enter_add_item'.translate(context),
                  ),
                  autofocus: true,
                  onEditingComplete: () => _addItem(context, isIncident),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new TextButton(
                    child: new Text('cancel'.translate(context)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  new TextButton(
                    child: new Text('ok'.translate(context)),
                    onPressed: () {
                      _addItem(context, isIncident);
                    },
                  ),
                ],
              ),
            ],
          );
        });
  }

  _addItem(BuildContext context, bool isIncident) {
    if (isIncident) {
      incidentsOnly!.add(_myController.text);
    } else {
      locationsOnly!.add(_myController.text);
    }
    ServerHelper.updateList(incidentsOnly!, locationsOnly!);
    Navigator.of(context).pop();
    updateLList();
  }
}
