import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/incident.dart';

class StackedBarChart extends StatelessWidget {
  final List<charts.Series<dynamic, String>> seriesList;
  final bool animate;

  StackedBarChart(this.seriesList, {this.animate = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        // width: 40,
        child: charts.BarChart(
      seriesList,
      animate: animate,
      barRendererDecorator: charts.BarLabelDecorator<String>(
          labelPosition: charts.BarLabelPosition.inside,
          insideLabelStyleSpec:
              charts.TextStyleSpec(color: charts.MaterialPalette.white)),
      // defaultRenderer: charts.BarRendererConfig(minBarLengthPx:  5),
      barGroupingType: charts.BarGroupingType.stacked,
    ));
  }

  static List<charts.Series<OrdinalIncidents, String>> createBarChart(
      BuildContext context, List<Incident> incidents) {
    int size = incidents.length;
    List<charts.Series<OrdinalIncidents, String>> result =
        List.empty(growable: true);
    for (String incident in incidentsOnly!) {
      List<OrdinalIncidents> list = List.empty(growable: true);
      for (String location in locationsOnly!) {
        int number = 0;
        String name = "";
        for (Incident idt in incidents) {
          if (idt.location == location && idt.titel == incident) {
            number += 1;
            name = incident;
          }
        }
        list.add(
            OrdinalIncidents(location, _getPercentage(number, size), name));
        // print(_getPercentage(number, size).toString() + " location" + location +
        //     " name" + name);
      }
      result.add(charts.Series<OrdinalIncidents, String>(
        id: "incident".translate(context),
        domainFn: (OrdinalIncidents it, _) => it.location,
        measureFn: (OrdinalIncidents it, _) => it.percentage,
        data: list,
        labelAccessorFn: (OrdinalIncidents incidents, __) =>
            '${getShort(incidents.incident)}\n' +
            ((incidents.percentage == 0)
                ? ""
                : '${incidents.percentage.toStringAsFixed(2)} %'),
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault.darker,
        fillColorFn: (_, __) => charts.MaterialPalette.red.shadeDefault.darker,
      ));
    }

    return result;
  }

  static double _getPercentage(int number, int size) {
    return ((100 / size) * number);
  }

  static String getShort(String titel) {
    String s = "ausfall";
    if (titel.contains(s)) {
      return titel.substring(0, titel.length - s.length);
    } else {
      return "";
    }
  }
}

class OrdinalIncidents {
  final String location;
  final String incident;
  final double percentage;

  OrdinalIncidents(this.location, this.percentage, this.incident);
}
