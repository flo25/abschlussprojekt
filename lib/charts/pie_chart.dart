import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/language/translations.dart';
import 'package:incident_manager/model/incident.dart';

class PieChartLabel extends StatelessWidget {
  final List<charts.Series<dynamic, String>> seriesList;
  final bool animate;

  PieChartLabel(this.seriesList, {this.animate = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.7,
        width: MediaQuery.of(context).size.width * 0.8,
        child: charts.PieChart<String>(
          seriesList,
          animate: animate,
          defaultRenderer: charts.ArcRendererConfig(arcRendererDecorators: [
            charts.ArcLabelDecorator(
                labelPosition: charts.ArcLabelPosition.inside)
          ]),
        ));
  }

  static List<charts.Series<LinearIncidents, String>> createData(
      List<Incident> incidents, BuildContext context) {
    List<LinearIncidents> data = List.empty(growable: true);
    int number = 0;
    int size = incidents.length;
    for (String titel in incidentsOnly!) {
      number = 0;
      for (Incident incident in incidents) {
        if (incident.titel == titel) {
          number += 1;
        }
      }
      data.add(new LinearIncidents(titel, _getPercentage(number, size)));
    }

    return [
      charts.Series<LinearIncidents, String>(
        id: "incident".translate(context),
        domainFn: (LinearIncidents incidents, _) => incidents.incident,
        measureFn: (LinearIncidents incidents, _) => incidents.percentage,
        data: data,
        labelAccessorFn: (LinearIncidents row, _) =>
            '${getShort(row.incident)}: ${row.percentage.toStringAsFixed(2)} %',
      )
    ];
  }

  static double _getPercentage(int number, int size) {
    return ((100 / size) * number);
  }

  static String getShort(String titel) {
    String s = "ausfall";
    if (titel.contains(s)) return titel.substring(0, titel.length - s.length);
    return "Fehler";
  }
}

class LinearIncidents {
  final String incident;
  final double percentage;

  LinearIncidents(this.incident, this.percentage);
}
