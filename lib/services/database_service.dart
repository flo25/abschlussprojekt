// import 'package:incident_manager/model/incident.dart';
// import 'package:incident_manager/model/llist.dart';
// import 'package:incident_manager/model/user.dart';
// import 'package:retrofit/http.dart';
// import 'package:dio/dio.dart';
//
//
// /*https://pub.dev/packages/retrofit*/
// @RestApi(baseUrl: "http://10.0.2.2:7894")
// abstract class DatabaseService {
//   // factory DatabaseService({String baseUrl}) =
//   //      _DatabaseService;
//
//   factory DatabaseService(Dio dio, {String baseUrl}) = _DatabaseService;
//
//   // factory DatabaseService(String baseUrl){
//   //   return DatabaseService(baseUrl);
//   // }
//
//    // DatabaseService instance(String baseUrl){
//    //  return DatabaseService(baseUrl: baseUrl);
//   // }
//
//
//   //LIST
//   @PATCH("/INCIDENT_MANAGEMENT/list/update")
//   Future<void> updateList(List<String> incidents, List<String> locations);
//
//   @GET("/INCIDENT_MANAGEMENT/list/get")
//   Future<LList> getList();
//
//   //USER
//   @POST("/INCIDENT_MANAGEMENT/users/create")
//   Future<void> createUser(@Body() User user);
//
//   @PATCH("/INCIDENT_MANAGEMENT/users/update/{uID}")
//   Future<void> updateUser(@Path() int id, @Body() Map<String, dynamic> map);
//
//   @GET("/INCIDENT_MANAGEMENT/users/get")
//   Future<User> getUser(@Queries() Map<String, dynamic> queries);
//
//   @GET("/INCIDENT_MANAGEMENT/users/gets")
//   Future<List<User>> getUsers(@Queries() Map<String, dynamic> queries);
//
//   @DELETE("/INCIDENT_MANAGEMENT/users/delete/{uID}")
//   Future<void> deleteUser(@Path() int id);
//
//   //INCIDENT
//   @POST("/INCIDENT_MANAGEMENT/incident")
//   Future<void> createIncident(@Body() Incident incident);
//
//   @PATCH("/INCIDENT_MANAGEMENT/incident/update/{incidentId}")
//   Future<void> updateIncident(@Path() int id, @Body() Map<String, dynamic> map);
//
//   @GET("/INCIDENT_MANAGEMENT/incident/get")
//   Future<Incident> getIncident(@Queries() Map<String, dynamic> queries);
//
//   @GET("/INCIDENT_MANAGEMENT/incident/gets")
//   Future<List<Incident>> getIncidents(@Queries() Map<String, dynamic> queries);
//
//   @DELETE("/INCIDENT_MANAGEMENT/incident/delete/{incidentId}")
//   Future<void> deleteIncident(@Path() int id);
// }
