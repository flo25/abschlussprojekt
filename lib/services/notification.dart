// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//
// //for info look at this site https://pub.dev/packages/flutter_local_notifications
//
// FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//     new FlutterLocalNotificationsPlugin();
// //todo icon für info und entwarnung anpassen
// // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
// var initializationSettingsAndroid =
// // new AndroidInitializationSettings(getNotificationIcon(isWarning)); fix for testing
//     new AndroidInitializationSettings('app_icon');
// var initializationSettingsIOS = new IOSInitializationSettings(
//     onDidReceiveLocalNotification: onDidReceiveLocalNotification);
// var initializationSettings = new InitializationSettings();
//
// Future onSelectNotification(String payload) async {
//   if (payload != null) {
//     print('notification payload: ' + payload);
//   }
//
//   flutterLocalNotificationsPlugin.initialize(initializationSettings,
//       onSelectNotification: onSelectNotification);
// }
//
// Future onDidReceiveLocalNotification(
//     int id, String title, String body, String payload) {
//   print('id: $id');
//   print('title: $title');
//   print('body: $body');
//   print('payload: $payload');
// }
//
// Future<void> showNotification() async {
//   var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//       'your channel id', 'your channel name', 'your channel description',
//       importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
//   var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//   var platformChannelSpecifics = NotificationDetails(
//       androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//   await flutterLocalNotificationsPlugin.show(
//       0, 'plain title', 'plain body', platformChannelSpecifics,
//       payload: 'item x');
// }
//
// Future<void> showWeeklyAtDayAndTime(Time time, int value, String title) async {
//   var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//       'show weekly channel id',
//       'show weekly channel name',
//       'show weekly description');
//   var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//   var platformChannelSpecifics = NotificationDetails(
//       androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//   await flutterLocalNotificationsPlugin.showWeeklyAtDayAndTime(
//       0,
//       title,
//       'Weekly notification shown on Monday at approximately ${_toTwoDigitString(time.hour)}:${_toTwoDigitString(time.minute)}:${_toTwoDigitString(time.second)}',
//       Day.values[value],
//       time,
//       platformChannelSpecifics);
// }
//
// Future<void> showDailyNotification(Time time) async {
//   var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//       'repeatDailyAtTime channel id',
//       'repeatDailyAtTime channel name',
//       'repeatDailyAtTime description');
//   var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//   var platformChannelSpecifics = new NotificationDetails(
//       androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//   await flutterLocalNotificationsPlugin.showDailyAtTime(
//       0,
//       'show daily title',
//       'Daily notification shown at approximately ${_toTwoDigitString(time.hour)}:${_toTwoDigitString(time.minute)}:${_toTwoDigitString(time.second)}',
//       time,
//       platformChannelSpecifics);
// }
//
// Future<void> checkPendingNotificationRequests() async {
//   var pendingNotificationRequests =
//       await flutterLocalNotificationsPlugin.pendingNotificationRequests();
//   for (var pendingNotificationRequest in pendingNotificationRequests) {
//     debugPrint(
//         'pending notification: [id: ${pendingNotificationRequest.id}, title: ${pendingNotificationRequest.title}, body: ${pendingNotificationRequest.body}, payload: ${pendingNotificationRequest.payload}]');
//   }
// }
//
// Future<void> cancelAllNotifications() async {
//   await flutterLocalNotificationsPlugin.cancelAll();
// }
//
// Future<void> scheduleNotification(int id, int minutes, String title) async {
//   var scheduledNotificationDateTime =
//       new DateTime.now().add(new Duration(minutes: minutes));
//   var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//       'your other channel id',
//       'your other channel name',
//       'your other channel description');
//   var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//   NotificationDetails platformChannelSpecifics = new NotificationDetails(
//       androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//   await flutterLocalNotificationsPlugin.schedule(
//       id, title, "", scheduledNotificationDateTime, platformChannelSpecifics);
// }
//
// String _toTwoDigitString(int value) {
//   return value.toString().padLeft(2, '0');
// }
//
// class PushNotification {
//   static FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
//       new FlutterLocalNotificationsPlugin();
//
//   PushNotification() {
//     var initializationSettingsAndroid =
//         new AndroidInitializationSettings('app_icon');
//     var initializationSettingsIOS = new IOSInitializationSettings(
//         onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//     var initializationSettings = new InitializationSettings(
//         initializationSettingsAndroid, initializationSettingsIOS);
//   }
//
//   static Future<void> onSelectNotification(String payload) async {
//     if (payload != null) {
//       print('notification payload: ' + payload);
//     }
//
//     _flutterLocalNotificationsPlugin.initialize(initializationSettings,
//         onSelectNotification: onSelectNotification);
//   }
//
//   static Future<void> onDidReceiveLocalNotification(
//       int id, String title, String body, String payload) async {
//     print('id: $id');
//     print('title: $title');
//     print('body: $body');
//     print('payload: $payload');
//   }
//
//   static Future<void> showNotification() async {
//     var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//         'your channel id', 'your channel name', 'your channel description',
//         importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
//     var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//     var platformChannelSpecifics = NotificationDetails(
//         androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//     await _flutterLocalNotificationsPlugin.show(
//         0, 'plain title', 'plain body', platformChannelSpecifics,
//         payload: 'item x');
//   }
//
//   static Future<void> showWeeklyAtDayAndTime(
//       Time time, int value, String title) async {
//     var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//         'show weekly channel id',
//         'show weekly channel name',
//         'show weekly description');
//     var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//     var platformChannelSpecifics = NotificationDetails(
//         androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//     await _flutterLocalNotificationsPlugin.showWeeklyAtDayAndTime(
//         0,
//         title,
//         'Weekly notification shown on Monday at approximately ${_toTwoDigitString(time.hour)}:${_toTwoDigitString(time.minute)}:${_toTwoDigitString(time.second)}',
//         Day.values[value],
//         time,
//         platformChannelSpecifics);
//   }
//
//   static Future<void> showDailyNotification(Time time) async {
//     var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//         'repeatDailyAtTime channel id',
//         'repeatDailyAtTime channel name',
//         'repeatDailyAtTime description');
//     var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//     var platformChannelSpecifics = new NotificationDetails(
//         androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//     await _flutterLocalNotificationsPlugin.showDailyAtTime(
//         0,
//         'show daily title',
//         'Daily notification shown at approximately ${_toTwoDigitString(time.hour)}:${_toTwoDigitString(time.minute)}:${_toTwoDigitString(time.second)}',
//         time,
//         platformChannelSpecifics);
//   }
//
//   static Future<void> checkPendingNotificationRequests() async {
//     var pendingNotificationRequests =
//         await _flutterLocalNotificationsPlugin.pendingNotificationRequests();
//     for (var pendingNotificationRequest in pendingNotificationRequests) {
//       debugPrint(
//           'pending notification: [id: ${pendingNotificationRequest.id}, title: ${pendingNotificationRequest.title}, body: ${pendingNotificationRequest.body}, payload: ${pendingNotificationRequest.payload}]');
//     }
//   }
//
//   static Future<void> scheduleNotification(
//       int id, int minutes, String title) async {
//     var scheduledNotificationDateTime =
//         new DateTime.now().add(new Duration(minutes: minutes));
//     var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//         'your other channel id',
//         'your other channel name',
//         'your other channel description');
//     var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//     NotificationDetails platformChannelSpecifics = new NotificationDetails(
//         androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//     await _flutterLocalNotificationsPlugin.schedule(
//         id, title, "", scheduledNotificationDateTime, platformChannelSpecifics);
//   }
//
//   static Future<void> cancelAllNotifications() async {
//     await _flutterLocalNotificationsPlugin.cancelAll();
//   }
// }
