import 'package:dio/dio.dart';
import 'package:incident_manager/helper/constants.dart';
import 'package:incident_manager/helper/db_strings.dart';
import 'package:incident_manager/model/incident.dart';
import 'package:incident_manager/model/llist.dart';
import 'package:incident_manager/model/user.dart';
import 'package:intl/intl.dart';

class ServerHelper {
  static final dio = Dio();
  static final dbService = DatabaseService(dio,
      baseUrl:
          "http://192.168.56.1:7896"); //10.0.2.2 localhost for android emulator
  //  Hostname: LAPTOP216.softvision.local alte Arbeit
  //Hostname aktuell:DESKTOP-28A51QF  IP aktuell:  192.168.56.1

  static final DateFormat dateFormatter = DateFormat("dd.MM.yyyy");
  static final DateFormat timeFormatter = DateFormat("HH:mm");

  static get instance {
    return ServerHelper();
  }

  //LIST

  static void updateList(List<String>? incidents, List<String>? locations) {
    dbService.updateList(incidents!, locations!).catchError((Object obj) {
      print("Got error updateList    " + obj.toString());
    });
  }

  static LList? list;

  static LList? getList() {
    dbService.getList().then((value) => list = value).catchError((Object obj) {
      print("Got error getList    " + obj.toString());
    });
    return list;
  }

  //USER
  static User? _user;

  static User? getUser(int uid) {
    Map<String, dynamic> map = Map();
    map[DBStrings.UID] = uid;
    dbService
        .getUser(map)
        .then((value) => {_user = value})
        .catchError((Object obj) {
      print("Got error getUser    " + obj.toString());
    });
    return _user;
  }

  static List<User>? user;

  static List<User>? getUsers(
      String? name, String? role, bool? notification, String? location) {
    Map<String, dynamic> map = Map();
    if (name != null && name.isNotEmpty) {
      map[DBStrings.U_NAME] = name;
    }
    if (location != null && location.isNotEmpty) {
      map[DBStrings.U_LOCATION] = location;
    }
    if (role != null) {
      map[DBStrings.U_ROLE] = role;
    }
    if (notification != null) {
      map[DBStrings.U_NOTIFICATION] = notification;
    }
    dbService
        .getUsers(map)
        .then((value) => user = value)
        .catchError((Object obj) {
      print("Got error getUsers    " + obj.toString());
    });
    return user;
  }

  static void createUser(int? id, String? name, String? role,
      bool? notification, String? location, String? token) {
    Map<String, dynamic> map = {
      DBStrings.UID: id,
      DBStrings.U_NAME: name,
      DBStrings.U_ROLE: role,
      DBStrings.U_NOTIFICATION: notification,
      DBStrings.U_LOCATION: location,
      DBStrings.U_TOKEN: token
    };
    dbService.createUser(map).catchError((Object obj) {
      print("Got error createUser    " + obj.toString());
    });
  }

  static void updateUser(int? uId, String? token, String? name, Role? role,
      bool? notification, String? location) {
    Map<String, dynamic> map = {
      DBStrings.UID: uId,
    };
    if (token != null && token.isNotEmpty) {
      map[DBStrings.U_TOKEN] = token;
    }
    if (name != null && name.isNotEmpty) {
      map[DBStrings.U_NAME] = name;
    }
    if (location != null && location.isNotEmpty) {
      map[DBStrings.U_LOCATION] = location;
    }
    if (role != null) {
      map[DBStrings.U_ROLE] = role;
    }
    if (notification != null) {
      map[DBStrings.U_NOTIFICATION] = notification;
    }
    dbService.updateUser(uId!, map).catchError((Object obj) {
      print("Got error updateUser    " + obj.toString());
    });
  }

  static deleteUser(int? uId) {
    dbService.deleteUser(uId!).catchError((Object obj) {
      print("Got error deleteUser    " + obj.toString());
    });
  }

  //INCIDENT
  static void createIncident(
      int? id,
      String? titel,
      String? description,
      String? location,
      DateTime startTime,
      DateTime endtime,
      DateTime idate,
      String? sender,
      bool? isWarning,
      int? adminId,
      int? sendTo,
      int? sendState) {
    Map<String, dynamic> map = {
      DBStrings.I_ID: id,
      DBStrings.I_TITEL: titel,
      DBStrings.I_DESCRIPTION: description,
      DBStrings.I_LOCATION: location,
      DBStrings.I_STARTTIME: timeFormatter.format(startTime),
      DBStrings.I_ENDTIME: timeFormatter.format(endtime),
      DBStrings.I_DATE: dateFormatter.format(idate),
      DBStrings.I_SENDER: sender,
      DBStrings.I_ISWARNING: isWarning,
      DBStrings.I_ADMINID: adminId,
      DBStrings.I_SENDTO: sendTo,
      DBStrings.I_SENDSTATE: sendState
    };
    dbService.createIncident(map).catchError((Object obj) {
      print("Got error createIncident    " + obj.toString());
    });
  }

  static void updateIncident(
      int? iId,
      String? titel,
      String? description,
      String? location,
      DateTime? startTime,
      DateTime? endtime,
      DateTime? date,
      String? sender,
      bool? isWarning,
      int? adminID,
      int? sendTo,
      int? sendState) {
    Map<String, dynamic> map = {
      DBStrings.I_ID: iId,
    };
    if (adminID != null && adminID != -1) {
      map[DBStrings.I_ADMINID] = adminID;
    }
    if (titel != null && titel.isNotEmpty) {
      map[DBStrings.I_TITEL] = titel;
    }
    if (description != null && description.isNotEmpty) {
      map[DBStrings.I_DESCRIPTION] = description;
    }
    if (location != null && location.isNotEmpty) {
      map[DBStrings.I_LOCATION] = location;
    }
    if (startTime != null) {
      map[DBStrings.I_STARTTIME] = timeFormatter.format(startTime);
    }
    if (endtime != null) {
      map[DBStrings.I_ENDTIME] = timeFormatter.format(endtime);
    }
    if (date != null) {
      map[DBStrings.I_DATE] = dateFormatter.format(date);
    }
    if (sender != null && sender.isNotEmpty) {
      map[DBStrings.I_SENDER] = sender;
    }
    if (isWarning != null) {
      map[DBStrings.I_ISWARNING] = isWarning;
    }
    if (sendTo != null && sendTo != -1) {
      map[DBStrings.I_SENDTO] = sendTo;
    }
    if (sendState != null && sendState != -1) {
      map[DBStrings.I_SENDSTATE] = sendState;
    }
    dbService.updateIncident(iId!, map).catchError((Object obj) {
      print("Got error updateIncident    " + obj.toString());
    });
  }

  static Incident? incident;

  static Incident? getIncident(int? iId) {
    Map<String, dynamic> map = Map();
    map[DBStrings.I_ID] = iId;
    dbService
        .getIncident(map)
        .then((value) => incident = value)
        .catchError((Object obj) {
      print("Got error getIncident    " + obj.toString());
    });
    return incident;
  }

  static List<Incident>? incidents;

  static List<Incident>? getIncidents(
      String? titel,
      String? description,
      String? location,
      DateTime? startTime,
      DateTime? endtime,
      DateTime? date,
      DateTime? startDate,
      DateTime? endDate,
      String? sender,
      bool? isWarning,
      int? adminID) {
    Map<String, dynamic> map = Map();
    if (adminID != null && adminID != -1) {
      map[DBStrings.I_ADMINID] = adminID;
    }
    if (titel != null && titel.isNotEmpty) {
      map[DBStrings.I_TITEL] = titel;
    }
    if (description != null && description.isNotEmpty) {
      map[DBStrings.I_DESCRIPTION] = description;
    }
    if (location != null && location.isNotEmpty) {
      map[DBStrings.I_LOCATION] = location;
    }
    if (startTime != null) {
      map[DBStrings.I_STARTTIME] = timeFormatter.format(startTime);
    }
    if (endtime != null) {
      map[DBStrings.I_ENDTIME] = timeFormatter.format(endtime);
    }
    if (date != null) {
      map[DBStrings.I_DATE] = dateFormatter.format(date);
    }
    //Filter
    if (startDate != null) {
      map[DBStrings.I_STARTDATE] = dateFormatter.format(startDate);
    }
    if (endDate != null) {
      map[DBStrings.I_ENDDATE] = dateFormatter.format(endDate);
    }
    if (sender != null && sender.isNotEmpty) {
      map[DBStrings.I_SENDER] = sender;
    }
    if (isWarning != null) {
      map[DBStrings.I_ISWARNING] = isWarning;
    }
    dbService
        .getIncidents(map)
        .then((value) => incidents = value)
        .catchError((Object obj) {
      print("Got error getIncidents    " + obj.toString());
    });
    return incidents;
  }

  static deleteIncident(int? iId) {
    dbService.deleteIncident(iId!).catchError((Object obj) {
      print("Got error deleteIncident    " + obj.toString());
    });
  }
}
