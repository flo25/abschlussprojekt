import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:incident_manager/page/splash_screen.dart';
import 'package:incident_manager/theme/theme_notifier.dart';
import 'package:incident_manager/theme/theme_value.dart';
import 'package:incident_manager/theme/themes.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'helper/constants.dart';
import 'helper/shared_pref.dart';
import 'language/application.dart';
import 'language/translations.dart';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  ThemeNotifier themeNotifier = ThemeNotifier(AppTheme().lightTheme);

  Future<Null> getThemeNotifier() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String theme = prefs.getString(Themes.APP_THEME)!;
    setState(() {
      if (theme == null || theme.isEmpty) {
        themeNotifier = ThemeNotifier(AppTheme().lightTheme);
      } else {
        themeNotifier = ThemeNotifier(theme == Themes.DARK
            ? AppTheme().darkTheme
            : AppTheme().lightTheme);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getThemeNotifier();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(themeNotifier.getTheme()),
      child: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late SpecificLocalizationDelegate _localOverrideDelegate;
  List<String> languages = ['de'];

  @override
  void initState() {
    super.initState();
    APPLIC.setSupportedLanguages(languages);
    _localOverrideDelegate = new SpecificLocalizationDelegate(null);
    applic.onLocaleChanged = onLocaleChange;
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _localOverrideDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    SharedPref.checkForFirstStart();
    loadLList();
    return MaterialApp(
      title: "Incident Manager",
      theme: themeNotifier.getTheme(),
      home: SplashScreen(),
      localizationsDelegates: [
        _localOverrideDelegate,
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: applic.supportedLocales(),
    );
  }
}
