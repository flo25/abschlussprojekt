import 'package:flutter/material.dart';

typedef void LocaleChangeCallback(Locale locale);

class APPLIC {
  //List of supported languages
  static List<String> supportLanguages = [];

  ///sets the supported languages in a list
  ///use formate en for English, de for German and so on
  ///example List<String> languages = ['de','en'];
  static setSupportedLanguages(List<String> mSupportedLanguages) {
    supportLanguages = mSupportedLanguages;
  }

  ///returns the possible supported languages
  get supportedLanguages => supportLanguages;

  //List of supported Locales
  Iterable<Locale> supportedLocales() =>
      supportLanguages.map<Locale>((lang) => new Locale(lang, ''));

  late LocaleChangeCallback onLocaleChanged;

  static final APPLIC _applic = new APPLIC._internal();

  factory APPLIC() {
    return _applic;
  }

  APPLIC._internal();
}

///instance of applic
APPLIC applic = new APPLIC();
