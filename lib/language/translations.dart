import 'dart:async' show Future;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'application.dart';

///Class for language translation in the App
class Translations {
  Translations(Locale locale) {
    this.locale = locale;
    _localizedValues = null;
  }

  late Locale locale;
  static Map<dynamic, dynamic>? _localizedValues;

  /// need context of current page
  static Translations? of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }

  /// the id from the value json file
  String text(String key) {
    return _localizedValues?[key] ?? '** $key not found';
  }

  ///load the specified locale json value file
  ///method load string from assets/i18n/${locale.languageCode}.json
  static Future<Translations> load(Locale locale) async {
    Translations translations = new Translations(locale);
    String jsonContent =
        await rootBundle.loadString('assets/i18n/${locale.languageCode}.json');
    _localizedValues = json.decode(jsonContent);
    return translations;
  }

  ///returns the current selected language
  get currentLanguage => locale.languageCode;
}

///extension to use a easier way to translate the strings
extension CustomTranslation on String {
  ///just take the id as string and use this method to translate
  ///e.g "username_hint".translate(context);
  String translate(BuildContext context) {
    return Translations.of(context)!.text(this);
  }
}

class TranslationsDelegate extends LocalizationsDelegate<Translations> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      applic.supportedLanguages.contains(locale.languageCode);

  @override
  Future<Translations> load(Locale locale) => Translations.load(locale);

  @override
  bool shouldReload(TranslationsDelegate old) => false;
}

class SpecificLocalizationDelegate extends LocalizationsDelegate<Translations> {
  final Locale? overriddenLocale;

  const SpecificLocalizationDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => overriddenLocale != null;

  @override
  Future<Translations> load(Locale locale) =>
      Translations.load(overriddenLocale!);

  @override
  bool shouldReload(LocalizationsDelegate<Translations> old) => true;
}
