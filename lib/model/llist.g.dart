// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'llist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LList _$LListFromJson(Map<String, dynamic> json) => LList(
      (json['incidents'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['locations'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$LListToJson(LList instance) => <String, dynamic>{
      'incidents': instance.incidents,
      'locations': instance.locations,
    };
