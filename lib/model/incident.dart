import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';

import 'llist.dart';
import 'user.dart';

part 'incident.g.dart';

@RestApi(baseUrl: "http://10.0.2.2:7896")
abstract class DatabaseService {
  factory DatabaseService(Dio dio, {String baseUrl}) = _DatabaseService;

  //LIST
  @PATCH("/INCIDENT_MANAGEMENT/list/update")
  Future<void> updateList(List<String> incidents, List<String> locations);

  @GET("/INCIDENT_MANAGEMENT/list/get")
  Future<LList> getList();

  //USER
  @POST("/INCIDENT_MANAGEMENT/users/create")
  Future<void> createUser(@Queries() Map<String, dynamic> queries);

  @PATCH("/INCIDENT_MANAGEMENT/users/update/{uID}")
  Future<void> updateUser(@Path() int id, @Queries() Map<String, dynamic> map);

  @GET("/INCIDENT_MANAGEMENT/users/get")
  Future<User> getUser(@Queries() Map<String, dynamic> queries);

  @GET("/INCIDENT_MANAGEMENT/users/gets")
  Future<List<User>> getUsers(@Queries() Map<String, dynamic> queries);

  @DELETE("/INCIDENT_MANAGEMENT/users/delete/{uID}")
  Future<void> deleteUser(@Path() int id);

  //INCIDENT
  @POST("/INCIDENT_MANAGEMENT/incident/create")
  Future<void> createIncident(@Queries() Map<String, dynamic> queries);

  @PATCH("/INCIDENT_MANAGEMENT/incident/update/{incidentId}")
  Future<void> updateIncident(@Path() int id, @Body() Map<String, dynamic> map);

  @GET("/INCIDENT_MANAGEMENT/incident/get")
  Future<Incident> getIncident(@Queries() Map<String, dynamic> queries);

  @GET("/INCIDENT_MANAGEMENT/incident/gets")
  Future<List<Incident>> getIncidents(@Queries() Map<String, dynamic> queries);

  @DELETE("/INCIDENT_MANAGEMENT/incident/delete/{incidentId}")
  Future<void> deleteIncident(@Path() int id);
}

@JsonSerializable()
class Incident {
  int? incidentID;
  String? titel;
  String? description;
  String? location;
  String startTime;
  String endTime;
  String date;
  String? sender;
  bool? warning;
  int? adminId;
  int? sendTo;
  int? sendState;

  Incident(
      this.incidentID,
      this.titel,
      this.description,
      this.location,
      this.startTime,
      this.endTime,
      this.date,
      this.sender,
      this.warning,
      this.adminId,
      this.sendTo,
      this.sendState);

  factory Incident.fromJson(Map<String, dynamic> json) =>
      _$IncidentFromJson(json);

  Map<String, dynamic> toJson() => _$IncidentToJson(this);
}
