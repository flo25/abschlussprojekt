import 'package:incident_manager/helper/constants.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

//https://pub.dev/packages/json_serializable
@JsonSerializable()
class User {
  int? uID;
  String? name;
  Role? role;
  bool? notification;
  String? location;
  String? token;

  User(
      {this.uID,
      this.name,
      this.notification,
      this.role,
      this.location,
      this.token});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
