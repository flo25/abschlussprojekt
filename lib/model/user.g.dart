// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      uID: json['uID'] as int?,
      name: json['name'] as String?,
      notification: json['notification'] as bool?,
      role: $enumDecodeNullable(_$RoleEnumMap, json['role']),
      location: json['location'] as String?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'uID': instance.uID,
      'name': instance.name,
      'role': _$RoleEnumMap[instance.role],
      'notification': instance.notification,
      'location': instance.location,
      'token': instance.token,
    };

const _$RoleEnumMap = {
  Role.ADMIN: 'ADMIN',
  Role.EMPLOYEE: 'EMPLOYEE',
};
