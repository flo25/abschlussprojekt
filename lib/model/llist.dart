import 'package:json_annotation/json_annotation.dart';

part 'llist.g.dart';

@JsonSerializable()
class LList {
  List<String>? incidents;
  List<String>? locations;

  LList(this.incidents, this.locations);

  factory LList.fromJson(Map<String, dynamic> json) => _$LListFromJson(json);

  Map<String, dynamic> toJson() => _$LListToJson(this);
}
